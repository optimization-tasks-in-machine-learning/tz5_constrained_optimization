from . import utils

utils.__doc__ = """
Подготовлено студентами Финансового университета при Правительстве Российской Федерации по дисциплине
Оптимизационные задачи в машинном обучении. Для ознакомления с README модуля откройте страницу
https://gitlab.com/optimization-tasks-in-machine-learning/tz5_constrained_optimization
"""
